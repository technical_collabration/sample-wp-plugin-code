<?php
/*
Plugin Name: learnupon
Plugin URI: http://www.solutionsofts.com
Description: This plugin used for using learnupon api.
Version: 1.0
License: GPL2
*/

require 'plugin-updates/plugin-update-checker.php';
$ExampleUpdateChecker = PucFactory::buildUpdateChecker(
	'http://solutionsofts.com/mario/info.json',
	__FILE__
);
function learnupon() {}
register_activation_hook( __FILE__, 'learnupon');

add_action( 'admin_menu', 'learn_menu' );
add_action('template_redirect', 'all_on_one');

function all_on_one () {
		$learn_username = get_option('learn_username');
		$learnApi_password = get_option('learnApi_password');
		$redirect_url  = get_option('learn_url');
		$redirecturl = get_option('learn_redirectURL_Menu');
		$current_user = wp_get_current_user();
		$enrolled = get_option('learn_redircturl_enrolled');
		$not_enrolled = get_option('learn_redircturl_notenrolled');
		$email = $current_user->user_email;
		$user =  $current_user->user_login;
		
		$key = get_option('learn_pass');
		$date  = time();
		$string = "USER=".$user."&TS=".$date."&KEY=".$key."";
		$string = md5($string);

if(isset($_GET['n_enroll'])){
header('location:https://'.$redirect_url.'.learnupon.com/sqsso?Email='.$email.'&SSOUsername='.$user.'&SSOToken='.$string.'&TS='.$date.'&redirect_uri='.$not_enrolled.'');
}
if(isset($_GET['enroll'])){
header('location:https://'.$redirect_url.'.learnupon.com/sqsso?Email='.$email.'&SSOUsername='.$user.'&SSOToken='.$string.'&TS='.$date.'&redirect_uri='.$enrolled.'');
}
if(isset($_GET['status']))
{
	if($_GET['status'] == 'in_progress' )
	{
	
		header('location:https://'.$redirect_url.'.learnupon.com/sqsso?Email='.$email.'&SSOUsername='.$user.'&SSOToken='.$string.'&TS='.$date.'&redirect_uri=/course_details/'.$_GET['id'].'');
		
	}
	if($_GET['status'] == 'in_active' )
	{
		header('location:https://'.$redirect_url.'.learnupon.com/sqsso?Email='.$email.'&SSOUsername='.$user.'&SSOToken='.$string.'&TS='.$date.'&redirect_uri=/store');
	}
}	

}
function learn_menu()
{
    add_menu_page( 'learn', 'LearnUpon', 'administrator','learn','viewPage');
}
function viewPage(){

	if ( !current_user_can( 'manage_options' ) )  {

		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );

	}	

		require_once("manage.php");

	}

	// Creating the widget

	class wpb_widget extends WP_Widget {

	function __construct() {

	parent::__construct(

	// Base ID of your widget

	'wpb_widget',

	// Widget name will appear in UI

	__('Learn Upon', 'wpb_widget_domain'),

	// Widget description

	array( 'description' => __( 'Learn Upon', 'wpb_widget_domain' ), )

	);

	}

	// Creating widget front-end

	// This is where the action happens

	public function widget( $args, $instance ) {
if(get_option('learn_check') == 1){
require_once("learnupon_widget.php");
		
		}
}

	// Widget Backend

	public function form( $instance ) {

	echo "Learn Upon";

	}

	// Updating widget replacing old instances with new

	public function update( $new_instance, $old_instance ) {

$instance = array();

	$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

	return $instance;

	}

	} // Class wpb_widget ends here

	// Register and load the widget

	function wpb_load_widget() {

	    register_widget( 'wpb_widget' );

	}

	add_action( 'widgets_init', 'wpb_load_widget' );

	// when user create 
	add_action( 'user_register', 'learn_registration_save', 10, 1 );

function learn_registration_save( $user_id ) {

$user_info = get_userdata($user_id);
$first_name = $user_info->data->display_name;
$email = $user_info->data->user_email;
$username = get_option('learn_username');
$pass = get_option('learn_pass');
$url  = get_option('learn_url');
$pass = get_option('learn_password');
$learn_username = get_option('learn_username');
$learnApi_password = get_option('learnApi_password');
$data = array("User"=> array("first_name" => $first_name,'email'=>$email,'password'=>$pass,'language'=>'en')); 
$content = json_encode($data);
$url = "https://".$url.".learnupon.com/api/v1/users";
ob_start();
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
curl_setopt($curl, CURLOPT_USERPWD, "".$learn_username.":".$learnApi_password."");
curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //curl error SSL certificate problem, verify that the CA cert is OK

curl_exec($curl);

curl_close($curl);
}
// update profile hook
add_action( 'profile_update', 'my_profile_update', 10, 2 );

    function my_profile_update( $user_id, $old_user_data ) {
		//update_option( 'update_profile','testing123');
		$user_info = get_userdata($user_id);
		$first_name = $user_info->first_name;
		$last_name = $user_info->last_name;
		$new_email = $user_info->data->user_email;
		$old_email = $old_user_data->user_email;
		$learn_username = get_option('learn_username');
		$learnApi_password = get_option('learnApi_password');
		$url  = get_option('learn_url');
		$pass = $_POST['pass1'];
		$data = array("User"=> array("last_name"=>$last_name,"first_name" => $first_name,'email'=>$old_email,"new_email"=>$new_email,'language'=>'en'));
		
		if(!empty($pass) && $pass!='')
		 $data['User']['password']=$pass ;
$content = json_encode($data);
$url = "https://".$url.".learnupon.com/api/v1/users/0";
ob_start();
$curl = curl_init($url);
//curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($curl, CURLOPT_USERPWD, $learn_username.":".$learnApi_password);
curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //curl error SSLcertificate problem, verify that the CA cert is OK
$result = curl_exec($curl);
$response   = json_decode($result);
curl_close($curl);
	
    }
// get password when user register	
add_filter('random_password', 'modify_the_pass');
function modify_the_pass($pass) {
    $pass = substr($pass, 0, 6); // make $pass six characters
	update_option( 'learn_password',$pass);
    return $pass; // return our new $pass
}
// update password
add_action( 'password_reset', 'my_password_reset', 10, 2 );
function my_password_reset( $user, $new_password ) {
  update_option( 'update_pass',$new_password);
}
// second widgets

	// Creating the widget

	class wpb_second_widget extends WP_Widget {

	function __construct() {

	parent::__construct(

	// Base ID of your widget

	'wpb_second_widget',

	// Widget name will appear in UI

	__('Learn Upon Enrolled', 'wpb_widget_domain'),

	// Widget description

	array( 'description' => __( 'Learn Upon for Enrolled user', 'wpb_widget_domain' ), )

	);

	}

	// Creating widget front-end

	// This is where the action happens

	public function widget( $args, $instance ) {

require_once("learnupon_second_widget.php");

}

	// Widget Backend

	public function form( $instance ) {

	echo "Learn Upon Enrolled";

	}

	// Updating widget replacing old instances with new

	public function update( $new_instance, $old_instance ) {

$instance = array();

	$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

	return $instance;

	}

	} // Class wpb_widget ends here

	// Register and load the widget

	function wpb_load_widget_second() {

	    register_widget( 'wpb_second_widget' );

	}

	add_action( 'widgets_init', 'wpb_load_widget_second' );



?>